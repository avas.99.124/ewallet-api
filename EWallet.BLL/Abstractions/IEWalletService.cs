﻿using EWalletApi.BLL.Models;

namespace EWalletApi.BLL.Abstractions
{
    public interface IEWalletService
    {
        Task<bool> UserHasEWallet(Guid userId);

        Task TopUp(Guid eWalletId, decimal amount);

        Task<TopUpCurrentMonthSummaryDto> TopUpCurrentMounthSummary();

        Task<decimal> GetEWalletBalance(Guid eWalletId);
    }
}
