﻿using AutoMapper;
using EWalletApi.BLL.Models;
using EWalletApi.DAL.Models.Output.EWallet;

namespace EWalletApi.BLL.Configuration.Automapper
{
    public class EWalletProfile : Profile
    {
        public EWalletProfile()
        {
            CreateMap<GetTotalTopUpCountAndAmountInPeriodByEWalletModel, EWalletTopUpCurrentMonthSummaryDto>();
        }
    }
}
