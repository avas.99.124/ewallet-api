﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EWalletApi.BLL.Configuration.Options
{
    public class EWalletBalanceConstraintOptions
    {
        public decimal Identified { get; set; }
        public decimal Unidentified { get; set; }
    }
}
