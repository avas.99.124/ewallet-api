﻿using EWalletApi.BLL.Abstractions;
using EWalletApi.BLL.Configuration.Automapper;
using EWalletApi.BLL.Configuration.Options;
using EWalletApi.BLL.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace EWalletApi.BLL
{
    public static class ServiceBuilder
    {
        public static void RegisterServices(IServiceCollection services, ConfigurationManager configuration)
        {
            
            //Automapper
            services.AddAutoMapper(typeof(EWalletProfile));
            services.AddScoped<IEWalletService, EWalletService>();

            DAL.ServiceBuilder.RegisterServices(services, configuration);

        }
    }
}
