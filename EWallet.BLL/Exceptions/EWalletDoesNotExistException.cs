﻿namespace EWalletApi.BLL.Exceptions
{
    public class EWalletDoesNotExistException : Exception
    {
        public Guid EWalletId { get; set; }
        public EWalletDoesNotExistException(string message, Guid id) : base(message)
        {
            EWalletId = id;
        }
    }
}
