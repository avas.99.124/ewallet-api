﻿namespace EWalletApi.BLL.Exceptions
{
    public class InvalidTopUpAmountException : Exception
    {
        public decimal ActualBalance { get; set; }
        public decimal MaxAvailableBalance { get; set; }
        public InvalidTopUpAmountException(string message, decimal actualBalance, decimal maxAvailableBalance) : base(message)
        {
            ActualBalance = actualBalance;
            MaxAvailableBalance = maxAvailableBalance;
        }
    }
}
