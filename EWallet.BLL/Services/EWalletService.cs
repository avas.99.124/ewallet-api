﻿using AutoMapper;
using EWalletApi.BLL.Abstractions;
using EWalletApi.BLL.Configuration.Options;
using EWalletApi.BLL.Exceptions;
using EWalletApi.BLL.Models;
using EWalletApi.DAL.Abstractions;
using Microsoft.Extensions.Options;

namespace EWalletApi.BLL.Services
{
    public class EWalletService : IEWalletService
    {
        private readonly IEWalletRepository _eWalletRepository;
        private readonly EWalletBalanceConstraintOptions _eWalletBalanceConstraintOptions;
        private readonly IMapper _mapper;
        public EWalletService(IEWalletRepository eWalletRepository, 
                              IMapper mapper,
                              IOptions<EWalletBalanceConstraintOptions> eWalletBalanceConstraintOptions)
        {
            _eWalletRepository = eWalletRepository ?? throw new ArgumentNullException(nameof(eWalletRepository));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _eWalletBalanceConstraintOptions = eWalletBalanceConstraintOptions?.Value ?? throw new ArgumentNullException(nameof(eWalletBalanceConstraintOptions));
        }

        private async Task CheckTopUpAmountIsValidAndThrowException(Guid eWalletId, decimal amount)
        {
            var eWallet = await _eWalletRepository.GetEWallet(eWalletId);

            if (eWallet is null)
                throw new EWalletDoesNotExistException("EWallet does not exist", eWalletId);

            if (eWallet.Identificated && amount + eWallet.Balance >= _eWalletBalanceConstraintOptions.Identified)
                throw new InvalidTopUpAmountException("TopUp amount is too big", eWallet.Balance, _eWalletBalanceConstraintOptions.Identified);

            if (!eWallet.Identificated && amount + eWallet.Balance >= _eWalletBalanceConstraintOptions.Unidentified)
                throw new InvalidTopUpAmountException("TopUp amount is too big", eWallet.Balance, _eWalletBalanceConstraintOptions.Unidentified);
        }

        public async Task<decimal> GetEWalletBalance(Guid eWalletId)
        {
            if (!(await _eWalletRepository.EWalletExists(eWalletId)))
                throw new EWalletDoesNotExistException("EWallet does not exist", eWalletId);

            return await _eWalletRepository.GetEWalletBalance(eWalletId);
        }

        public async Task TopUp(Guid eWalletId, decimal amount)
        {
            if (!(await _eWalletRepository.EWalletExists(eWalletId)))
                throw new EWalletDoesNotExistException("EWallet does not exist", eWalletId);

            await CheckTopUpAmountIsValidAndThrowException(eWalletId, amount);

            await _eWalletRepository.TopUp(eWalletId, amount);
        }

        public async Task<TopUpCurrentMonthSummaryDto> TopUpCurrentMounthSummary()
        {
            var result = new TopUpCurrentMonthSummaryDto();
            result.StartDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1, 0, 0, 0);
            result.EndDate = DateTime.Now;

            var topUpSummary = await _eWalletRepository.GetTotalTopUpCountAndAmountInPeriod(result.StartDate, result.EndDate);

            result.TopUpCurrentMonthSummary = _mapper.Map<IEnumerable<EWalletTopUpCurrentMonthSummaryDto>>(topUpSummary);

            result.TotalAmount = result.TopUpCurrentMonthSummary.Sum(eWalletSummary => eWalletSummary.TotalAmount);
            result.TotalCount = result.TopUpCurrentMonthSummary.Sum(eWalletSummary => eWalletSummary.TotalCount);

            return result;
        }

        public async Task<bool> UserHasEWallet(Guid userId)
        {
            return await _eWalletRepository.UserHasEWallet(userId);
        }
    }
}
