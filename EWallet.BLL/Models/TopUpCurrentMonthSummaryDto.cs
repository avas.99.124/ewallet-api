﻿namespace EWalletApi.BLL.Models
{
    public class TopUpCurrentMonthSummaryDto
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public IEnumerable<EWalletTopUpCurrentMonthSummaryDto> TopUpCurrentMonthSummary { get; set; }
        public int TotalCount { get; set; }
        public decimal TotalAmount { get; set; }
    }
    public class EWalletTopUpCurrentMonthSummaryDto
    {
        public Guid EWalletId { get; set; }
        public decimal TotalAmount { get; set; }
        public int TotalCount { get; set; }
    }
}
