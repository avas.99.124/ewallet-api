﻿namespace EWalletApi.BLL.Models.EWallet
{
    public class EWalletDto
    {
        public Guid Id { get; set; }
        public decimal Balance { get; set; }
        public bool Identificated { get; set; }
    }
}
