﻿using DbUp;
using Microsoft.Extensions.Configuration;

var config = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .Build();

var connectionString = args.FirstOrDefault() ?? config.GetConnectionString("DataBaseConnectionString");


var sciriptsPath = config.GetSection("ScriptsPath").Value;


EnsureDatabase.For.SqlDatabase(connectionString);

var upgrader =
    DeployChanges.To
        .SqlDatabase(connectionString)
        .WithScriptsFromFileSystem(sciriptsPath)
        .LogScriptOutput()
        .LogToConsole()
        .Build();

var result = upgrader.PerformUpgrade();

if (!result.Successful)
{
    Console.ForegroundColor = ConsoleColor.Red;
    Console.WriteLine(result.Error);
    Console.ResetColor();

#if DEBUG
    Console.ReadLine();
#endif

    return -1;
}

Console.ForegroundColor = ConsoleColor.Green;
Console.WriteLine("Success!");
Console.ResetColor();

Console.ReadLine();

return 0;


