﻿CREATE OR ALTER PROCEDURE [dbo].[GetEWalletBalance]
	@EWalletId uniqueidentifier
AS
BEGIN
	SELECT Balance FROM [dbo].[EWalletBalance] WHERE EWalletId = @EWalletId
END
