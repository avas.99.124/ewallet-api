﻿IF object_id('[dbo].[EWallet]', 'U') IS NULL
BEGIN 
	CREATE TABLE [dbo].[EWallet]
	(
		[Id] uniqueidentifier NOT NULL DEFAULT NEWID(),
		[UserId] uniqueidentifier Not NULL,
		[Identificated] BIT NOT NULL,
		[CreationDate] DATETIME
		
		CONSTRAINT PK_EWallet_Id PRIMARY KEY (Id)
	);
END
