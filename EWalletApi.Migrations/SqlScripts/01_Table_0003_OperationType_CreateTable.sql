﻿IF object_id('[dbo].[OperationType]', 'U') IS NULL
BEGIN 
	CREATE TABLE [dbo].[OperationType]
	(
		[Id] int NOT NULL identity(1,1),
		[Name] nvarchar(100),
		[CreationDate] DATETIME
		
		CONSTRAINT PK_OperationType_Id PRIMARY KEY (Id)
	);
END
