﻿CREATE OR ALTER PROCEDURE [dbo].[GetTotalTopUpCountAndAmountInPeriod]
	@StartDate DATETIME,
	@EndDAte DATETIME
AS
BEGIN
	
	SELECT EWalletId as EWalletId,
		   Count(Amount) as TotalCount,
		   Sum(Amount) as TotalAmount
	FROM [dbo].[EWalletHistory] 
	WHERE 1=1
	AND OperationDate BETWEEN @StartDate AND @EndDate
	AND OperationTypeId = 1
	GROUP BY EWalletId

END
