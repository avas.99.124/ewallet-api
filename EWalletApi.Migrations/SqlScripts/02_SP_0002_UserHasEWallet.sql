﻿CREATE OR ALTER PROCEDURE [dbo].[UserHasEWallet]
	@UserId uniqueidentifier
AS
BEGIN
	SELECT COUNT(1) FROM [dbo].[EWallet] WHERE UserId = @UserId
END
