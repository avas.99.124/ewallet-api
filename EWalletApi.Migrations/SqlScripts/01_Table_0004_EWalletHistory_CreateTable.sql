﻿IF object_id('[dbo].[EWalletHistory]', 'U') IS NULL
BEGIN 
	CREATE TABLE [dbo].[EWalletHistory]
	(
		[EWalletId] uniqueidentifier NOT NULL,
		[Amount] Decimal(28,2) Not NULL,
		[OperationTypeId] INT NOT NULL,
		[OperationDate] DATETIME
		
		CONSTRAINT FK_EWalletHistory_EWalletId_EWallet_Id FOREIGN KEY (EWalletId) REFERENCES [dbo].[EWallet] (Id),
		CONSTRAINT FK_EWalletHistory_OperationTypeId_OperationType_Id FOREIGN KEY (OperationTypeId) REFERENCES [dbo].[OperationType] (Id)
	);
END
