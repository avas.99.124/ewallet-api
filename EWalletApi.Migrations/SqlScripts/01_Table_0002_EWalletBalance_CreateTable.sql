﻿IF object_id('[dbo].[EWalletBalance]', 'U') IS NULL
BEGIN 
	CREATE TABLE [dbo].[EWalletBalance]
	(
		[EWalletId] uniqueidentifier NOT NULL,
		[Balance] Decimal(28,2) Not NULL,
		[LastUpdateDate] DATETIME
		
		CONSTRAINT FK_EWalletAccount_EWalletId_EWallet_Id FOREIGN KEY (EWalletId) REFERENCES [dbo].[EWallet] (Id)
	);
END
