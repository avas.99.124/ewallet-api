﻿CREATE OR ALTER PROCEDURE [dbo].[GetEWAllet]
	@EWalletId uniqueidentifier
AS
BEGIN
	
	SELECT ew.Id as Id,
		   ewb.Balance as Balance,
		   ew.Identificated as Identificated
	FROM [dbo].[EWallet] ew
	JOIN [dbo].[EWalletBalance] ewb
	ON ew.Id = ewb.EWalletId
	WHERE ew.Id = @EWalletId

END
