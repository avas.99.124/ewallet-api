﻿CREATE OR ALTER PROCEDURE [dbo].[EWalletTopUp]
	@EWalletId uniqueidentifier,
	@Amount Decimal(28,2)
AS
BEGIN
	
	DECLARE @TopUpDateTime DATETIME = GETDATE();

	INSERT INTO [dbo].[EWalletHistory](EWalletId,Amount,OperationTypeId,OperationDate)
	VALUES (@EWalletId, @Amount, 1, @TopUpDateTime);

	DECLARE @CurrentBalance Decimal(28,2) = (SELECT Balance FROM [dbo].[EWAlletBalance] WHERE EWalletId = @EWalletId);

	UPDATE [dbo].[EWAlletBalance]
	SET Balance = @CurrentBalance + @Amount, LastUpdateDate = @TopUpDateTime
	Where EWalletId = @EWalletId
END
