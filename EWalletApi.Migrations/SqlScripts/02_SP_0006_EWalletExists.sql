﻿CREATE OR ALTER PROCEDURE [dbo].[EWalletExists]
	@EWalletId uniqueidentifier
AS
BEGIN
	
	SELECT Count(1)
	FROM [dbo].[EWallet] 
	WHERE Id = @EWalletId

END

