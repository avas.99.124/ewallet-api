﻿namespace EWalletApi.Models.EWallet.Response
{
    public class GetBalanceResponseDto
    {
        public decimal Balance { get; set; }
    }
}
