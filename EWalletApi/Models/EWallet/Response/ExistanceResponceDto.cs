﻿namespace EWalletApi.Models.EWallet.Response
{
    public class ExistanceResponceDto
    {
        public bool Exists { get; set; }
    }
}
