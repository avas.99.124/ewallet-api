﻿using System.ComponentModel.DataAnnotations;

namespace EWalletApi.Models.EWallet.Request
{
    public class TopUpRequestDto
    {
        [Required]
        public Guid EWalletId { get; set; }

        [Required]
        public decimal Amount { get; set; }
    }
}
