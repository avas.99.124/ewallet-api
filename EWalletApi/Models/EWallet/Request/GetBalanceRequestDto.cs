﻿using System.ComponentModel.DataAnnotations;

namespace EWalletApi.Models.EWallet.Request
{
    public class GetBalanceRequestDto
    {
        [Required]
        public Guid EWalletId { get; set; }
    }
}
