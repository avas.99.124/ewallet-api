﻿namespace EWalletApi.Constants
{
    public static class ErrorsTypes
    {
        public const string Prefix = "https://alif.uz/http-errors/";
        public const string BadRequest = $"{Prefix}400";
        public const string Unauthorized = $"{Prefix}401";
        public const string Forbidden = $"{Prefix}403";
        public const string NotFound = $"{Prefix}404";
        public const string MethodNotAllowed = $"{Prefix}405";
        public const string NotAcceptable = $"{Prefix}406";
        public const string RequestTimeout = $"{Prefix}408";
        public const string Conflict = $"{Prefix}409";
        public const string PreconditionFailed = $"{Prefix}412";
        public const string UnsupportedMediaType = $"{Prefix}415";
        public const string UnprocessableEntity = $"{Prefix}422";
        public const string UpgradeRequired = $"{Prefix}426";
        public const string InternalServerError = $"{Prefix}500";
        public const string BadGateway = $"{Prefix}502";
        public const string ServiceUnavailable = $"{Prefix}503";
        public const string GatewayTimeout = $"{Prefix}504";

        public const string EWalletApiErrorPrefix = "https://alif.uz/ewallet-api/";
        public const string UnexpectedError = $"{EWalletApiErrorPrefix}unexpected";
        public const string InvalidTopUpAmountError = $"{EWalletApiErrorPrefix}top-up-ammount-invalid";
        public const string EWalletDoesNotExistError = $"{EWalletApiErrorPrefix}ewallet-does-not-exists";
    }
}
