﻿namespace EWalletApi.Constants
{
    public static class CustomHeaders
    {
        public const string XDigest = "X-Digest";
        public const string XUserId = "X-UserId";
    }
}
