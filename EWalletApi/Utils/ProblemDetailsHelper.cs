﻿using Microsoft.AspNetCore.Mvc;

namespace EWalletApi.Utils
{
    public class ProblemDetailsHelper
    {
        public static ProblemDetails CreateProblemDetailsWithExtensions(
        string? type = null,
        string? title = null,
        int? statusCode = null,
        string? detail = null,
        Dictionary<string, object?>? extensions = null)
        {
            var problemDetails = new ProblemDetails
            {
                Detail = detail,
                Status = statusCode ?? 500,
                Title = title,
                Type = type,
            };

            if (extensions is not null)
                foreach (var item in extensions)
                    problemDetails.Extensions.Add(item);

            return problemDetails;
        }

    }
}
