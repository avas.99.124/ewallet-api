﻿using EWalletApi.Abstractions;
using EWalletApi.BLL.Exceptions;
using EWalletApi.Constants;
using Microsoft.AspNetCore.Mvc;

namespace EWalletApi.Utils
{
    public class ExceptionToProblemDetailsMapper : IExceptionToProbemDetailsMapper
    {
        //Для более большого проекта уместно вынести в нугет и сделать всё через рефлексию
        public ProblemDetails MapExceptionsToProblemDetails(Exception exception)
        {
            if (exception.GetType() == typeof(InvalidTopUpAmountException))
            {
                var ex = (InvalidTopUpAmountException)exception;
                return ProblemDetailsHelper.CreateProblemDetailsWithExtensions(
                        type: ErrorsTypes.InvalidTopUpAmountError,
                        title: "Top up error",
                        statusCode: 400,
                        detail: exception.Message,
                        extensions: new Dictionary<string, object?>
                        {
                            { "ActualBalance", ex.ActualBalance },
                            { "MaxAvialableBalance", ex.MaxAvailableBalance }
                        });
            }

            if (exception.GetType() == typeof(EWalletDoesNotExistException))
            {
                var ex = (EWalletDoesNotExistException)exception;
                return ProblemDetailsHelper.CreateProblemDetailsWithExtensions(
                        type: ErrorsTypes.EWalletDoesNotExistError,
                        title: "EWallet does not exist error",
                        statusCode: 400,
                        detail: exception.Message,
                        extensions: new Dictionary<string, object?>
                        {
                            { "EWalletId", ex.EWalletId }
                        });
            }

            return new ProblemDetails()
            {
                Detail = exception.Message,
                Status = 500,
                Title = "Unexepcted error",
                Type = ErrorsTypes.UnexpectedError
            };
        }
    }
}
