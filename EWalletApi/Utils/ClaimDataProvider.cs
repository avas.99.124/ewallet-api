﻿using EWalletApi.Abstractions;
using EWalletApi.Constants;
using System.Security.Claims;

namespace EWalletApi.Utils
{
    public class ClaimDataProvider : IClaimDataProvider
    {
        private string GetClaimData(ClaimsPrincipal user, string claimName)
        {
            if (user is null)
                throw new ArgumentNullException(nameof(user));

            if (string.IsNullOrWhiteSpace(claimName))
                throw new ArgumentNullException(nameof(claimName));

            if (user.Identities is null)
                throw new ArgumentNullException(nameof(user.Identities));

            if (user.Identity is not ClaimsIdentity claimsIdentity)
                throw new ArgumentException(nameof(user.Identity));

            if (claimsIdentity.Claims is null)
                throw new ArgumentNullException(nameof(claimsIdentity.Claims));

            var claim = claimsIdentity.Claims.FirstOrDefault(c => c.Type == claimName);

            if (claim is null)
                throw new InvalidDataException($"Claim named {claimName} was not found");

            return claim.Value;
        }

        public Guid GetUserId(ClaimsPrincipal user)
        {
            if (user is null)
                throw new ArgumentNullException(nameof(user));

            var userId = GetClaimData(user, ClaimsNames.UserId);

            return new Guid(userId);

        }
    }
}
