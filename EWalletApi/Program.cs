using EWalletApi.Abstractions;
using EWalletApi.Authentication;
using EWalletApi.BLL;
using EWalletApi.BLL.Configuration.Options;
using EWalletApi.Constants;
using EWalletApi.Options;
using EWalletApi.Utils;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Configuration;

var builder = WebApplication.CreateBuilder(args);

ServiceBuilder.RegisterServices(builder.Services, builder.Configuration);

//Add Options
builder.Services.AddOptions<HmacOptions>().Bind(builder.Configuration.GetRequiredSection(nameof(HmacOptions)));
builder.Services.AddOptions<EWalletBalanceConstraintOptions>().Bind(builder.Configuration.GetRequiredSection(nameof(EWalletBalanceConstraintOptions)));
builder.Services.AddSingleton<IClaimDataProvider, ClaimDataProvider>();
builder.Services.AddSingleton<IExceptionToProbemDetailsMapper, ExceptionToProblemDetailsMapper>();

builder.Services.AddAuthentication("HmacAuthentication")
                .AddScheme<AuthenticationSchemeOptions, HmacAuthenticationHandler>
                ("HmacAuthentication", null);
builder.Services.AddAuthorization(options =>
{
    options.DefaultPolicy = new AuthorizationPolicyBuilder()
            .AddAuthenticationSchemes("HmacAuthentication")
            .RequireAuthenticatedUser()
            .Build();
});
builder.Services.AddControllers();
builder.Services.AddProblemDetails(options =>
{
    options.CustomizeProblemDetails = (context) =>
    {
        if (!context.ProblemDetails.Type?.StartsWith(ErrorsTypes.Prefix) ?? true)
            context.ProblemDetails.Type = context.ProblemDetails.Status switch
            {
                StatusCodes.Status400BadRequest => ErrorsTypes.BadRequest,
                StatusCodes.Status401Unauthorized => ErrorsTypes.Unauthorized,
                StatusCodes.Status403Forbidden => ErrorsTypes.Forbidden,
                StatusCodes.Status404NotFound => ErrorsTypes.NotFound,
                StatusCodes.Status405MethodNotAllowed => ErrorsTypes.MethodNotAllowed,
                StatusCodes.Status406NotAcceptable => ErrorsTypes.NotAcceptable,
                StatusCodes.Status408RequestTimeout => ErrorsTypes.RequestTimeout,
                StatusCodes.Status409Conflict => ErrorsTypes.Conflict,
                StatusCodes.Status412PreconditionFailed => ErrorsTypes.PreconditionFailed,
                StatusCodes.Status415UnsupportedMediaType => ErrorsTypes.UnsupportedMediaType,
                StatusCodes.Status422UnprocessableEntity => ErrorsTypes.UnprocessableEntity,
                StatusCodes.Status426UpgradeRequired => ErrorsTypes.UpgradeRequired,
                StatusCodes.Status500InternalServerError => ErrorsTypes.InternalServerError,
                StatusCodes.Status502BadGateway => ErrorsTypes.BadGateway,
                StatusCodes.Status503ServiceUnavailable => ErrorsTypes.ServiceUnavailable,
                StatusCodes.Status504GatewayTimeout => ErrorsTypes.GatewayTimeout,
                _ => $"{ErrorsTypes.Prefix}unknown"
            };
    };
});
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
app.UseExceptionHandler("/error");

app.UseSwagger();
app.UseSwaggerUI();

//app.UseHttpsRedirection();

app.UseAuthentication();
app.UseAuthorization();


app.MapControllers();


app.Run();
