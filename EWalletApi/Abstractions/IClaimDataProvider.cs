﻿using System.Security.Claims;

namespace EWalletApi.Abstractions
{
    public interface IClaimDataProvider
    {
        Guid GetUserId(ClaimsPrincipal user);
    }
}
