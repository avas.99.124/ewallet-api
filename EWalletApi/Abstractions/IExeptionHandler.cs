﻿using Microsoft.AspNetCore.Mvc;

namespace EWalletApi.Abstractions
{
    public interface IExceptionToProbemDetailsMapper
    {
        ProblemDetails MapExceptionsToProblemDetails(Exception exception);
    }
}
