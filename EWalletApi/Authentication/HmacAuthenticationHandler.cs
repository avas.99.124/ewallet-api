﻿using EWalletApi.Constants;
using EWalletApi.Options;
using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Options;
using System.Buffers;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Text.Encodings.Web;

namespace EWalletApi.Authentication
{
    public class HmacAuthenticationHandler : AuthenticationHandler<AuthenticationSchemeOptions>
    {
        private readonly HmacOptions _hmacOptions;
        public HmacAuthenticationHandler(
        IOptionsMonitor<AuthenticationSchemeOptions> options,
        ILoggerFactory logger,
        UrlEncoder encoder,
        ISystemClock clock,

        IOptions<HmacOptions> hmacOptions
        ) : base(options, logger, encoder, clock)
        {
            _hmacOptions = hmacOptions?.Value ?? throw new ArgumentNullException(nameof(hmacOptions));
        }

        protected override async Task<AuthenticateResult> HandleAuthenticateAsync()
        {
            var xDigitHeaderValue = Request.Headers[CustomHeaders.XDigest].FirstOrDefault();
            if (xDigitHeaderValue is null)
                return AuthenticateResult.Fail($"{CustomHeaders.XDigest} header does not exist");


            var xUserIdHeaderValue = Request.Headers[CustomHeaders.XUserId].FirstOrDefault();
            if (xUserIdHeaderValue is null)
                return AuthenticateResult.Fail($"{CustomHeaders.XUserId} header does not exist");

            var requestIsValid = await IsValidRequest(xDigitHeaderValue);

            if (!requestIsValid)
                return AuthenticateResult.Fail("Hash sum is not valid");

            var claims = new[] { new Claim(ClaimsNames.UserId, xUserIdHeaderValue) };
            var identity = new ClaimsIdentity(claims, "hmac1");
            var claimsPrincipal = new ClaimsPrincipal(identity);
            return AuthenticateResult.Success(new AuthenticationTicket(claimsPrincipal, Scheme.Name));

        }

        private async Task<bool> IsValidRequest(string hashedRequestBody)
        {
            Request.EnableBuffering();
            var secretKeyBytes = Encoding.UTF8.GetBytes(_hmacOptions.Secret);


            var bodyBuffer = (await Request.BodyReader.ReadAsync()).Buffer;


            using (var hmac = new HMACSHA1(secretKeyBytes))
            {
                var h = hmac.ComputeHash(bodyBuffer.ToArray());
                var hash = Convert.ToHexString(h).ToUpper();
                Request.Body.Position = 0;
                return hash.Equals(hashedRequestBody.ToUpper());
            }






        }
    }
}
