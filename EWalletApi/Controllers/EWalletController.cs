﻿using EWalletApi.Abstractions;
using EWalletApi.BLL.Abstractions;
using EWalletApi.Models.EWallet.Request;
using EWalletApi.Models.EWallet.Response;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace EWalletApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class EWalletController : ControllerBase
    {
        private readonly IEWalletService _eWalletService;
        private readonly IClaimDataProvider _claimDataProvider;
        public EWalletController(IEWalletService eWalletService,
                                 IClaimDataProvider claimDataProvider)
        {
            _eWalletService = eWalletService ?? throw new ArgumentNullException(nameof(eWalletService));
            _claimDataProvider = claimDataProvider ?? throw new ArgumentNullException(nameof(claimDataProvider));
        }

        [HttpPost("Existance")]
        public async Task<IActionResult> EWalletExistance()
        {
            var userId = _claimDataProvider.GetUserId(User);

            var exists = await _eWalletService.UserHasEWallet(userId);

            return Ok(new ExistanceResponceDto() { Exists = exists});
        }

        [HttpPost("TopUp")]
        public async Task<IActionResult> EWalletTopUp(TopUpRequestDto topUpRequestDto)
        {
            await _eWalletService.TopUp(topUpRequestDto.EWalletId, topUpRequestDto.Amount);

            return Ok();
        }

        [HttpPost("TopUpCurrentMounthSummary")]
        public async Task<IActionResult> TopUpCurrentMounthSummary()
        {
            return Ok(await _eWalletService.TopUpCurrentMounthSummary());
        }

        [HttpPost("Balance")]
        public async Task<IActionResult> GetBalance(GetBalanceRequestDto getBalanceRequestDto)
        {
            var balance = await _eWalletService.GetEWalletBalance(getBalanceRequestDto.EWalletId);

            return Ok(new GetBalanceResponseDto() { Balance = balance });
        }

    }
}
