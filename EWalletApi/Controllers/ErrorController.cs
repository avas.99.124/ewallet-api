﻿using EWalletApi.Abstractions;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Mvc;

namespace EWalletApi.Controllers
{
    [ApiExplorerSettings(IgnoreApi = true)]
    public class ErrorsController : ControllerBase
    {
        private readonly IExceptionToProbemDetailsMapper _exceptionToProbemDetailsMapper;
        public ErrorsController(IExceptionToProbemDetailsMapper exceptionToProbemDetailsMapper)
        {
            _exceptionToProbemDetailsMapper = exceptionToProbemDetailsMapper ?? throw new ArgumentNullException(nameof(exceptionToProbemDetailsMapper));
        }

        [Route("/error")]
        public IActionResult Error()
        {
            var ex = HttpContext.Features.Get<IExceptionHandlerFeature>()?.Error;
            if (ex is null)
                return Problem();

            return new ObjectResult(_exceptionToProbemDetailsMapper.MapExceptionsToProblemDetails(ex));
        }

    }
}
