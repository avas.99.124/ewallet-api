﻿using System.Data;

namespace EWalletApi.DAL.Abstractions
{
    public interface IDbConnectionFactory
    {
        IDbConnection Create();
    }
}
