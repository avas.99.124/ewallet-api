﻿using EWalletApi.DAL.Models.Output.EWallet;

namespace EWalletApi.DAL.Abstractions
{
    public interface IEWalletRepository
    {
        Task<bool> EWalletExists(Guid eWalletId);
        Task<GetEWalletModel> GetEWallet(Guid eWalletId);
        Task<bool> UserHasEWallet(Guid userId);
        Task TopUp(Guid eWalletId, decimal amount);
        Task<IEnumerable<GetTotalTopUpCountAndAmountInPeriodByEWalletModel>> GetTotalTopUpCountAndAmountInPeriod(DateTime startDate, DateTime endDate);
        Task<decimal> GetEWalletBalance(Guid eWalletId);
    }
}
