﻿namespace EWalletApi.DAL.Models.Output.EWallet
{
    public class GetEWalletModel
    {
        public Guid Id { get; set; }
        public decimal Balance { get; set; }
        public bool Identificated { get; set; }
    }
}
