﻿namespace EWalletApi.DAL.Models.Output.EWallet
{
    public class GetTotalTopUpCountAndAmountInPeriodByEWalletModel
    {
        public Guid EWalletId { get; set; }
        public decimal TotalAmount { get; set; }
        public int TotalCount { get; set; }
    }
}
