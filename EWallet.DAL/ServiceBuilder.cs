﻿using EWalletApi.DAL.Abstractions;
using EWalletApi.DAL.Repositories;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace EWalletApi.DAL
{
    public static class ServiceBuilder
    {
        public static void RegisterServices(IServiceCollection services, ConfigurationManager configuration)
        {
            //Database
            var connectionString = configuration.GetConnectionString("EWalletApi")
                                   ?? throw new ArgumentNullException("connectionString", "ConnectionString cannot be null");
            services.AddSingleton<IDbConnectionFactory>(sp => new DbConnectionFactory(connectionString));


            //Repositories
            services.AddTransient<IEWalletRepository, EWalletSqlRepository>();

        }
    }
}
