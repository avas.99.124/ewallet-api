﻿using EWalletApi.DAL.Abstractions;
using Microsoft.Data.SqlClient;
using System.Data;

namespace EWalletApi.DAL.Repositories
{
    public class DbConnectionFactory : IDbConnectionFactory
    {
        private readonly string _connectionString;

        public DbConnectionFactory(string connectionString)
        {
            if (string.IsNullOrWhiteSpace(connectionString))
                throw new ArgumentNullException(nameof(connectionString));
            _connectionString = connectionString;
        }

        public IDbConnection Create()
        {
            var conn = new SqlConnection(_connectionString);
            conn.Open();
            return conn;
        }

    }
}
