﻿using Dapper;
using EWalletApi.DAL.Abstractions;
using EWalletApi.DAL.Models.Output.EWallet;
using System.Data;

namespace EWalletApi.DAL.Repositories
{
    public class EWalletSqlRepository : IEWalletRepository
    {
        private readonly IDbConnectionFactory _dbConnectionFactory;
        public EWalletSqlRepository(IDbConnectionFactory dbConnectionFactory)
        {
            _dbConnectionFactory = dbConnectionFactory ?? throw new ArgumentNullException(nameof(dbConnectionFactory));
        }

        public async Task<bool> EWalletExists(Guid eWalletId)
        {
            using var db = _dbConnectionFactory.Create();

            return await db.QueryFirstAsync<bool>(sql: "[dbo].[EWalletExists]",
                param: new
                {
                    eWalletId
                },
                commandType: CommandType.StoredProcedure);
        }

        public async Task<GetEWalletModel> GetEWallet(Guid eWalletId)
        {
            using var db = _dbConnectionFactory.Create();

            return await db.QueryFirstAsync<GetEWalletModel>(sql: "[dbo].[GetEWallet]",
                param: new
                {
                    eWalletId
                },
                commandType: CommandType.StoredProcedure);
        }

        public async Task<decimal> GetEWalletBalance(Guid eWalletId)
        {
            using var db = _dbConnectionFactory.Create();

            return await db.QueryFirstAsync<decimal>(sql: "[dbo].[GetEWalletBalance]",
                param: new
                {
                    eWalletId
                },
                commandType: CommandType.StoredProcedure);
        }

        public async Task<IEnumerable<GetTotalTopUpCountAndAmountInPeriodByEWalletModel>> GetTotalTopUpCountAndAmountInPeriod(DateTime startDate, DateTime endDate)
        {
            using var db = _dbConnectionFactory.Create();

            return await db.QueryAsync<GetTotalTopUpCountAndAmountInPeriodByEWalletModel>(sql: "[dbo].[GetTotalTopUpCountAndAmountInPeriod]",
                param: new
                {
                    startDate,
                    endDate
                },
                commandType: CommandType.StoredProcedure);

        }

        public async Task TopUp(Guid eWalletId, decimal amount)
        {
            using var db = _dbConnectionFactory.Create();

            await db.ExecuteAsync(sql: "[dbo].[EWalletTopUp]",
                param: new
                {
                    eWalletId,
                    amount
                },
                commandType: CommandType.StoredProcedure);
        }

        public async Task<bool> UserHasEWallet(Guid userId)
        {
            using var db = _dbConnectionFactory.Create();

            return await db.QueryFirstAsync<bool>(sql: "[dbo].[UserHasEWallet]",
                param: new
                {
                    userId
                },
                commandType: CommandType.StoredProcedure);
        }
    }
}
